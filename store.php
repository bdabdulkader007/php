
<?php
/*
echo "<pre>";
var_dump($_POST);


echo "<hr />";

var_dump($_FILES);

echo "<hr />";*/

$tmp_name =  $_FILES['image']['tmp_name'];
$name =  $_FILES['image']['name'];

move_uploaded_file($tmp_name, 'uploads/'.$name);

/*
 * 01. form method = post,
 * 02. check name attribute in input fields
 * 03. checkbox name = "skill[]" // array define
 * 04. image attribute enctype="multipart/form-data"
 *
 * var_dump($_FILES);
 *
 * move_uploaded_file($tmp_name, 'uploads/'.$name);
 * */

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Skill</th>
                        <th>Department</th>
                        <th>Image</th>
                        <th>About</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $_POST['name']?></td>
                        <td><?php echo $_POST['gender']?></td>
                        <td><?php echo implode(', ',$_POST['skill'])?></td>
                        <td><?php echo $_POST['department']?></td>
                        <td>
                            <img width="250" src="uploads/<?php echo $name?>" alt="">
                        </td>
                        <td><?php echo $_POST['about']?></td>
                    </tr>
                </tbody>
            </table>
            <a href="create.php" class="btn btn-success">Back</a>
        </div>
    </div>
</div>
</body>
</html>