<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="store.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name">
                </div>
                <div class="form-group">
                    <div>
                        <label for="gender">Gender</label>
                    </div>
                    <label>
                        <input type="radio" name="gender" value="Male"> Male
                    </label>
                    <label>
                        <input type="radio" name="gender" value="Female"> Female
                    </label>
                </div>
                <div class="form-group">
                    <div>
                        <label>Skill</label>
                    </div>
                    <label>
                        <input type="checkbox" name="skill[]" value="php"> PHP
                    </label>

                    <label>
                        <input type="checkbox" name="skill[]" value="css"> CSS
                    </label>
                    <label>
                        <input type="checkbox" name="skill[]" value="js"> Js
                    </label>
                </div>

                <div class="form-group">
                    <label for="department">Department</label>
                    <select name="department" id="" class="form-control">
                        <option value="cse">CSE</option>
                        <option value="bba">BBA</option>
                        <option value="eee">EEE</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Write something</label>
                    <textarea class="form-control" name="about" id="" cols="30" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Upload Image</label>
                    <input type="file" name="image">
                </div>

                <button class="btn btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>